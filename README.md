tracksync
=========

This Python script aids in synchronizing multimedia tracks recorded
from different sources at the same time.

Please see 'sample-workflow.txt' for an explanation on how to use.

