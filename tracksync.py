#!/usr/bin/python3
#
#    tracksync.py, part of tracksync
#    Copyright (C) 2019  Octavio Alvarez
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from functools import reduce
import argparse
import os.path
import operator
import subprocess
import json


# In the future, use these extensions to detect (sort of) audio-only files
# and auto-supress video output
#audio_only_extensions = ['m4a', 'mp3', 'flac', 'ogg']

parser = argparse.ArgumentParser(description="Video and audio track aligner", epilog="syncspec is in the form [pointname:]timespec")
parser.add_argument("-t", "--track", action="append", nargs="+", help="list of input_tracks with sync timestamps in seconds")
parser.add_argument("-s", "--syncref", help="track against which track sync points are compared for alignment calculations", metavar='file')
parser.add_argument("-c", "--clockref", help="track which will not be stretched", metavar='file')
parser.add_argument("-o", "--add-avoutput", action="append", nargs=3, help="additional output (by default all channels are split)", metavar=('a:track', 'v:track', 'file'))
parser.add_argument("-k", "--kill-output", action="append", nargs=2, help="supresses this channel from the output", metavar=["file", "channel"])
parser.add_argument("-ss", "--start", help="start time in seconds (respecting syncref)", required=True, type=float)
parser.add_argument("-ee", "--end", help="end time in seconds (respecting syncref)", required=True, type=float)
args = parser.parse_args()

print()
print("Provided arguments:")
print(args)
print()

avoutputs = []
if args.add_avoutput:
    avoutputs = args.add_avoutput

clockref = args.track[0][0]
if args.clockref is not None:
    clockref = args.clockref

syncref = args.track[0][0]
if args.syncref is not None:
    syncref = args.syncref

def delay2adelay(time):
    return str(time) + "|" + str(time)

def time2sec(time):
    components = time.split(":")
    ac = 0
    for c in components:
        ac = 60 * ac + float(c)
    return ac

class InputTrack:
    def __init__(self, filename, index):
        self.filename = filename
        self.syncpoints = {}
        self.scale = 1.0
        self.baseoffset = 0.0
        self.outputoffset = 0.0
        self.index = index
        self.aproperties = {}
        self.vproperties = {}
        self.cproperties = {}
        self.ffprobe()
        self.vduration = float(self.get_vduration())
        
    def get_vduration(self):
        if "tags" in self.vproperties and "DURATION" in self.vproperties["tags"]:
            return float(time2sec(self.vproperties["tags"]["DURATION"]))

        if "duration" in self.cproperties:
            return float(self.cproperties["duration"])

        return None
        

    def ffprobe(self):
        ffprobe_call = "ffprobe -v error -show_format -show_streams -of json".split(" ")
        ffprobe_call.append(self.filename)
        result = json.loads(subprocess.check_output(ffprobe_call))

        self.cproperties = result['format']
    
        for stream in result['streams']:
            if stream['codec_type'] == "video":        
                self.vproperties = stream
            if stream['codec_type'] == "audio":
                self.aproperties = stream

    def __repr__(self):
        return "<InputTrack {}: {}; {}>".format(self.filename, self.get_timesorted_syncpoint_list(), [self.scale, self.baseoffset, self.outputoffset])
        
    def add_syncpoint(self, syncpoint_name, syncpoint_value):
        self.syncpoints[syncpoint_name] = syncpoint_value
        
    def get_scaled_syncpoint(self, syncpoint_name):
        return self.syncpoints[syncpoint_name] * self.scale
        
    def get_timesorted_syncpoint_list(self):
        return sorted(self.syncpoints.items(), key=operator.itemgetter(1))

    def parse_syncpointspec(self, syncpointspec, default_syncpoint_name = "other"):
        syncpoint = syncpointspec.split(":")
        if len(syncpoint) == 1:
            self.add_syncpoint(default_syncpoint_name, float(syncpoint[0]))
        elif len(syncpoint) == 2:
            self.add_syncpoint(syncpoint[0], float(syncpoint[1]))
        else:
            raise ValueError

    def calculate_scale(self, point_names, basetrack):
        delta_self = self.syncpoints[point_names[1]] - self.syncpoints[point_names[0]]
        delta_base = basetrack.syncpoints[point_names[1]] - basetrack.syncpoints[point_names[0]]
        return delta_base / delta_self

    def calculate_offset(self, syncin_point_name, basetrack):
        return basetrack.syncpoints[syncin_point_name] - self.syncpoints[syncin_point_name] * self.scale

class AudioOutputTrack:
    def __init__(self, atrack):
        self.atrack = atrack

    def __repr__(self):
        return "<AudioOutputTrack: {}>".format({'atrack': self.atrack.filename})

    def get_filter(self):
        afilter_elements = []

        if self.atrack.scale != 1:
            atempofilter = "atempo={}".format(1 / self.atrack.scale)
            afilter_elements.append(atempofilter)

        if self.atrack.outputoffset != 0:
            adelayfilter = "adelay={}".format(delay2adelay(1000 * self.atrack.outputoffset))
            afilter_elements.append(adelayfilter)

        afilter_elements.append("apad")
        afilter = ",".join(afilter_elements)

        if afilter == "":
            afilter = "null"
        
        return afilter
        
    def get_commandline(self, start, duration):
        command = "ffmpeg -y -hide_banner"
        inputs = "-i {}".format(self.atrack.filename)
        filter_complex = "-filter_complex '[0:a]{}[aout]'".format(self.get_filter())
        mapping = "-map \"[aout]\""
        ss = "-ss {}".format(start)
        t = "-t {}".format(duration)
        filename_components = os.path.splitext(os.path.basename(self.atrack.filename))
        output_filename = "track-{}-audio.flac".format(filename_components[0])
        return " ".join([command, inputs, filter_complex, mapping, ss, t, output_filename])

class VideoOutputTrack:
    def __init__(self, vtrack):
        self.vtrack = vtrack

    def __repr__(self):
        return "<VideoOutputTrack: {}>".format({'vtrack': self.vtrack.filename})

    def get_video_filtercomplex(self):
        return "[{}:v]null[video]"

    def get_frame_length(self):
        avg_frame_rate = self.vtrack.vproperties["avg_frame_rate"]
        nd = avg_frame_rate.split("/") # nd: num/denom in fps
        n = nd[0]
        d = 1
        if len(nd) == 2:
            d = nd[1]
        return int(d) / int(n)

    def is_video_scaling_worth_it(self):
        syncpoint_list = self.vtrack.get_timesorted_syncpoint_list()
        return (syncpoint_list[1][1] - syncpoint_list[0][1]) * (1/self.vtrack.scale - 1) >= self.get_frame_length()

    def get_vscale_filter_part(self):
        if self.is_video_scaling_worth_it():
            return "*{}".format(self.vtrack.scale)
        
        return ""

    def get_commandline(self, start, duration):
        command = "ffmpeg -y -hide_banner"
        inputs = "-i {}".format(self.vtrack.filename)
        filter_complex = "-filter_complex '[0:v]trim=start_frame=0:end_frame=1,geq=0:128:128,loop=-1:1,split[zs1][za];[zs1]trim=start=0:end={}[zb];[0:v]setpts=(PTS-STARTPTS){}[scaled];[zb][scaled][za]concat=n=3:v=1[vout]'".format(self.vtrack.outputoffset, self.get_vscale_filter_part())
        mapping = "-map \"[vout]\""
        ss = "-ss {}".format(start)
        t = "-t {}".format(duration)
        filename_components = os.path.splitext(os.path.basename(self.vtrack.filename))
        output_filename = "-crf 0 -preset veryfast -c:v h264 -intra track-{}-video.mkv".format(filename_components[0])
        return " ".join([command, inputs, filter_complex, mapping, ss, t, output_filename])

class AudioVideoOutputTrack:
    def __init__(self, atrack, vtrack, output_filename):
        self.atrack = atrack
        self.vtrack = vtrack
        self.filename = output_filename

    def __repr__(self):
        return "<AudioVideoOutputTrack: {}>".format({'atrack': self.atrack.filename, 'vtrack': self.vtrack.filename})

    def get_afilter(self):
        afilter_elements = []

        if self.atrack.scale != 1:
            atempofilter = "atempo={}".format(1 / self.atrack.scale)
            afilter_elements.append(atempofilter)

        if self.atrack.outputoffset != 0:
            adelayfilter = "adelay={}".format(delay2adelay(1000 * self.atrack.outputoffset))
            afilter_elements.append(adelayfilter)

        afilter_elements.append("apad")

        afilter = ",".join(afilter_elements)

        if afilter == "":
            afilter = "null"
        
        return afilter

    def get_frame_length(self):
        avg_frame_rate = self.vtrack.vproperties["avg_frame_rate"]
        nd = avg_frame_rate.split("/") # nd: num/denom in fps
        n = nd[0]
        d = 1
        if len(nd) == 2:
            d = nd[1]
        return int(d) / int(n)

    def is_video_scaling_worth_it(self):
        syncpoint_list = self.vtrack.get_timesorted_syncpoint_list()
        return (syncpoint_list[1][1] - syncpoint_list[0][1]) * (1/self.vtrack.scale - 1) >= self.get_frame_length()

    def get_vscale_filter_part(self):
        if self.is_video_scaling_worth_it():
            return "*{}".format(self.vtrack.scale)
        
        return ""

    def get_commandline(self, start, duration):
        command = "ffmpeg -y -hide_banner"
        inputs = "-i {} -i {}".format(self.vtrack.filename, self.atrack.filename)
        filter_complex = "-filter_complex '[0:v]trim=start_frame=0:end_frame=1,geq=0:128:128,loop=-1:1,split[zs1][za];[zs1]trim=start=0:end={}[zb];[0:v]setpts=(PTS-STARTPTS){}[scaled];[zb][scaled][za]concat=n=3:v=1[vout];[1:a]{}[aout]'".format(self.vtrack.outputoffset, self.get_vscale_filter_part(), self.get_afilter())
        mapping = "-map \"[vout]\" -map \"[aout]\""
        ss = "-ss {}".format(start)
        t = "-t {}".format(duration)
        filename_components = os.path.splitext(os.path.basename(self.vtrack.filename))
        output_flags = "-crf 0 -preset veryfast -c:v h264 -intra"
        return " ".join([command, inputs, filter_complex, mapping, ss, t, output_flags, self.filename])
        
n = 0
default_syncpoint_names = ["I", "O"]
input_tracks = {}
for t in args.track:
    new_track = InputTrack(t[0], n)
    for p in range(1, len(t)):
        if p <= 2:
            new_track.parse_syncpointspec(t[p], default_syncpoint_names[p-1])
        else:
            new_track.parse_syncpointspec(t[p])
    input_tracks[t[0]] = new_track
    n = n + 1

input_tracks[syncref].add_syncpoint("ss", args.start)
input_tracks[syncref].add_syncpoint("ee", args.end)

print("InputTrack list:")
print(input_tracks)
print("Clock reference: " + clockref)
print("Sync reference: " + syncref)
print()

# First, scale syncref and move all "other" syncpoints to the clockref track.
if syncref != clockref:
    print("Scaling {} to {}...".format(syncref, clockref))
    input_tracks[syncref].scale = input_tracks[syncref].calculate_scale(["I", "O"], input_tracks[clockref])

    print("Moving other {} syncpoints to {}...".format(syncref, clockref))
    syncI = input_tracks[syncref].syncpoints["I"] * input_tracks[syncref].scale
    clockI = input_tracks[clockref].syncpoints["I"]
    
    for p in filter(lambda item: item[0] not in ["I", "O"], input_tracks[syncref].syncpoints.items()):
        print("- {}...".format(p[0]))
        input_tracks[clockref].add_syncpoint(p[0], p[1] * input_tracks[syncref].scale - syncI + clockI)
    input_tracks[syncref].syncpoints = dict(filter(lambda item: item[0] in ["I", "O"], input_tracks[syncref].syncpoints.items()))

print()
print("InputTrack list:")
print(input_tracks)
print()

# Now we can just sync all other input_tracks to clockref.
print("Syncing all other input_tracks...")
for t in [name for name in input_tracks.keys() if name not in [clockref]]:
    print("- {}".format(t))
    pointlist = [p[0] for p in input_tracks[t].get_timesorted_syncpoint_list()]
    print(pointlist)
    input_tracks[t].scale = input_tracks[t].calculate_scale(pointlist, input_tracks[clockref])
    print(input_tracks[t].scale)

print()

# Now calculate offsets based on clockref (some may result negative, will
# normalize in a later step).
print ("Calculating offsets with respect to clockref...")
for t in [name for name in input_tracks.keys()]:
    print("- {}".format(t))
    syncin_point_name = input_tracks[t].get_timesorted_syncpoint_list()[0][0]
    input_tracks[t].baseoffset = input_tracks[t].calculate_offset(syncin_point_name, input_tracks[clockref])
    print(input_tracks[t].baseoffset)

print()

# Normalize offsets
print("Normalizing offsets...")
min_offset = min([t.baseoffset for t in input_tracks.values()])
print("- Global offset: {}".format(-min_offset))
for t in input_tracks.keys():
    input_tracks[t].outputoffset = input_tracks[t].baseoffset - min_offset

print()
print("InputTrack list:")
print(input_tracks)
print()

# Prepare outputs
print("Preparing output input_tracks...")
n = 0
output_tracks = []
for t in input_tracks.keys():
    
    channels = [x for x in ["audio", "video"] if x not in [ko[1] for ko in args.kill_output if ko[0] == t]]

    # FIXME: Use the ffprobed-properties to automatically suppress tracks that don't have them.

    if "audio" in channels:
        new_track = AudioOutputTrack(input_tracks[t])
        output_tracks.append(new_track)

    if "video" in channels:
        new_track = VideoOutputTrack(input_tracks[t])
        output_tracks.append(new_track)

    n = n + 1

for output in avoutputs:
    a = output[0]
    v = output[1]
    f = output[2]

    new_track = AudioVideoOutputTrack(input_tracks[a], input_tracks[v], f)
    output_tracks.append(new_track)

print()
print("OutputTrack list:")
print(output_tracks)
print()

start = input_tracks[clockref].syncpoints["ss"] - min_offset
end = input_tracks[clockref].syncpoints["ee"] - min_offset
duration = end - start

for o in output_tracks:
    print(o.get_commandline(start, duration))

