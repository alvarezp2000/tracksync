#!/usr/bin/python3
#
#    timerec.py, part of tracksync
#    Copyright (C) 2019  Octavio Alvarez
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import time
import sys
import os

# Input:
#
# Empty line: ignored
# <string>: Adds a new entry, set to <string>
# *: Erases last entry
# *<string>: Replaces last entry with <string>

# The main idea is that the user plays all tracks + timerec.py simultaneously
# and records camera selection by entering "1" or "2" in order to have a set
# of timestamps of when a camera should be chosen. Then use ffmpeg to render
# this selection.

# Note that in humans the first camera is 1 but for ffmpeg the first camera
# would be zero. Also, timerec output is of no use for ffmpeg's sendcmd filter
# so the timerec2sendcmd.gawk script is useful to convert.

starttime = time.time()
instruction_number = 0
entries = []

for linein in sys.stdin:
    linein = linein.rstrip()

    if not linein:
        continue

    if linein == "*":
        lineout = "{0:>6} {1:>13.8f}".format(instruction_number, current_time - starttime)
        print("- {0}\n".format(lineout), file=sys.stderr)
        entries.pop()
        continue

    if linein[0] == "*":
        lineout = "{0:>6} {1:>13.8f}  {2}".format(instruction_number, current_time - starttime, linein[1:])
        print("* {0}\n".format(lineout), file=sys.stderr)
        entries.pop()
        entries.append((current_time - starttime, linein[1:]));
        continue

    instruction_number = instruction_number + 1
    current_time = time.time()
    lineout = "{0:>6} {1:>13.8f}  {2}".format(instruction_number, current_time - starttime, linein)
    print("+ {0}\n".format(lineout), file=sys.stderr)
    entries.append((current_time - starttime, linein));

instruction_number = 0
for (time, desc) in entries:
    instruction_number = instruction_number  + 1
    print(": {0:>6} {1:>13.8f}  {2}".format(instruction_number, time, desc), file=sys.stderr)
    print(": {0:>6} {1:>13.8f}  {2}".format(instruction_number, time, desc))

