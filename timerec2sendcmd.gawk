#!/usr/bin/gawk -f
#
#    timerec2sendcmd.gawk, part of tracksync
#    Copyright (C) 2019  Octavio Alvarez
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Camera selection (1-based)
/^: +[0-9]+ +[0-9.]+  [0-9]+$/ {
    if ($4 > 0)
        # Convert to zero-based and put appropriate output
        printf "%s streamselect map %d\n", $3, $4 - 1
}

